const express = require ('express')
const mongoose = require ('mongoose')
const dotenv = require('dotenv')

dotenv.config()

const app = express ()
const port = 3001
//MongoDB connection
mongoose.connect(`mongodb+srv://JEDBAGUNU:${process.env.MONGODB_PASSWORD}@cluster0.bxtf324.mongodb.net/?retryWrites=true&w=majority`,{
    useNewUrlParser: true,
    useUnifiedTopology: true
})
let db = mongoose.connection

db.on('error',()=>console.error('Connection error'))
db.on('open',()=>console.log('Connected to MongoDB!'))
//MongoDB connection end

app.use(express.json())
app.use(express.urlencoded({extended:true}))


const userSchema = new mongoose.Schema({
    username: String,
   password: String
})


const User = mongoose.model('User', userSchema)



app.post('/signup', (request, response)=>{
    User.findOne({username: request.body.username},(error, result)=>{
        if(result != null && result.username == request.body.username && result.password == request.body.password){
            return response.send('User Already exist!')
        }
        let newUser = new User({
            username: request.body.username,
            password: request.body.password
        })
        newUser.save((error, newUser)=>{
                if(error){
                    return console.error(error)
                }
                else {
                    return  response.status(200).send('New User Registered!')
                }
        })
    })

})
//Routes end

app.listen(port, ()=>console.log(`Server is running at local host: ${port}`))